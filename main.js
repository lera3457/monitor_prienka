// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import moment from 'moment'
Vue.prototype.$moment = moment

export var globalStore = new Vue({
  data: {
    timeIntervalInMilliseconds: 300000,
    isLoading: false,
    showErrorModal: false,
    role: 1
  }
})

Vue.filter('splitNumber', function (value) {
  return value ? value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ') : value
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
})
