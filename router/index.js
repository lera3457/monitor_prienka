import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/page/Home'
import AcceptanceSchedule from '@/components/page/AcceptanceSchedule'
import Status from '@/components/page/Status'
import Acceptance from '@/components/page/Acceptance'
import Schedule from '@/components/page/Schedule'
import Transport from '@/components/page/Transport'
import TransportStatus from '@/components/page/TransportStatus'
import AcceptanceChart from '@/components/page/AcceptanceChart'
import AcceptanceStatus from '@/components/page/AcceptanceStatus'
import AcceptanceTransportPUO from '@/components/page/AcceptanceTransportPUO'
import AcceptanceTransportSAP from '@/components/page/AcceptanceTransportSAP'
import Pallets from '@/components/page/Pallets'
import AcceptanceChartGate from '@/components/page/AcceptanceChartGate'
import Alert from '@/components/page/Alert'
import Alerts from '@/components/page/Alerts'
import AlertsDashboard from '@/components/page/AlertsDashboard'
import OccupancyTiers from '@/components/page/OccupancyTiers'
import FreeTierCells from '@/components/page/FreeTierCells'
import OccupancyCategories from '@/components/page/OccupancyCategories'
import FreeCategoryCells from '@/components/page/FreeCategoryCells'
import EmptyApp from '@/components/page/EmptyApp'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      meta: {
        title: 'Стартовая'
      }
    },
    {
      path: '/error',
      name: 'EmptyApp',
      component: EmptyApp,
      meta: {
        title: 'Сообщение об ошибке'
      }
    },
    {
      path: '/acceptance-schedule',
      component: Schedule,
      children: [
        {
          path: '',
          name: 'AcceptanceSchedule',
          component: AcceptanceSchedule,
          meta: {
            filterAcceptanceSchedule: true,
            title: 'График поставок'
          }
        },
        {
          path: 'gate',
          name: 'AcceptanceChartGate',
          component: AcceptanceChartGate,
          meta: {
            filterAcceptanceChartGate: true,
            title: 'График поставок'
          }
        }
      ]
    },
    {
      path: '/acceptance-chart',
      component: Acceptance,
      children: [
        {
          path: '',
          name: 'AcceptanceChart',
          component: AcceptanceChart,
          props (route) {
            return { switch: route.query.switch }
          },
          meta: {
            filterAcceptanceChart: true,
            title: 'График приемки по часам'
          }
        },
        {
          path: 'pallets',
          name: 'Pallets',
          component: Pallets,
          meta: {
            filterPallets: true,
            title: 'Паллет в STAGE'
          }
        }
      ]
    },
    {
      path: '/acceptance-status',
      component: Status,
      children: [
        {
          path: '',
          name: 'AcceptanceStatus',
          component: AcceptanceStatus,
          meta: { title: 'Статусы приемки' }
        },
        {
          path: 'transport-status',
          component: Transport,
          children: [
            {
              path: '',
              name: 'TransportStatus',
              component: TransportStatus,
              meta: {
                filterAcceptanceStatus: true,
                title: 'Статусы заказов на приёмке'
              }
            },
            {
              path: 'puo',
              name: 'AcceptanceTransportPUO',
              component: AcceptanceTransportPUO,
              meta: {
                filterAcceptanceStatusPuo: true,
                title: 'Ожидают приемки ТС с отклонением по началу операции'
              }
            },
            {
              path: 'sap',
              name: 'AcceptanceTransportSAP',
              component: AcceptanceTransportSAP,
              meta: {
                filterAcceptanceStatusPuo: true,
                title: 'Ожидают приемки ТС с отклонением по началу операции'
              }
            }
          ]
        }
      ]
    },
    {
      path: '/alerts-dashboard',
      component: Alert,
      children: [
        {
          path: '',
          name: 'AlertsDashboard',
          component: AlertsDashboard,
          meta: {
            filterAlerts: true,
            title: 'Алерты и предупреждения'
          }
        },
        {
          path: 'table',
          name: 'Alerts',
          component: Alerts,
          meta: {
            filterAlerts: true,
            title: 'Алерты и предупреждения'
          }
        }
      ]
    },
    {
      path: '/occupancy-tiers',
      name: 'OccupancyTiers',
      component: OccupancyTiers,
      meta: {
        filterTempZone: true,
        title: 'Размещение и хранение'
      }
    },
    {
      path: '/free-tier-cells',
      name: 'FreeTierCells',
      component: FreeTierCells,
      props: true,
      meta: {
        title: 'ячейки в ярусах',
        cell: true
      }
    },
    {
      path: '/occupancy-categories',
      name: 'OccupancyCategories',
      component: OccupancyCategories,
      meta: {
        filterTempZone: true,
        title: 'Размещение и хранение'
      }
    },
    {
      path: '/free-category-cells',
      name: 'FreeCategoryCells',
      component: FreeCategoryCells,
      props: true,
      meta: {
        title: 'ячейки в весовых категориях',
        cell: true
      }
    }
  ],
  linkActiveClass: 'active'
})
