import Repository from './common/Repository'
import qs from 'qs'

const resource = '/monitor_1_1'

export default {
  getScheduleByHour (ts) { return Repository.get(`${resource}?ts=${ts}`) },
  getStorage (queryParams) {
    return Repository.get(`${resource}`, {
      params: queryParams,
      paramsSerializer: params => {
        return qs.stringify(params, {arrayFormat: 'repeat'})
      }
    })
  }
}
