import Repository from './Repository'

const resource = '/config'

export default {
  get () { return Repository.get(`${resource}`) },
  put (body) { return Repository.put(`${resource}`, body) }
}
