import AcceptanceChartRepository from '../AcceptanceChartRepository'
import AcceptanceScheduleRepository from '../AcceptanceScheduleRepository'
import AcceptanceStatus from '../AcceptanceStatusRepository'
import TransportStatusRepository from '../TransportStatusRepository'
import AcceptanceTransportPUO from '../AcceptanceTransportPUORepository'
import AcceptanceTransportSAP from '../AcceptanceTransportSAPRepository'
import HomeRepository from '../HomeRepository'
import UtilsRepository from '../UtilsRepository'
import RealDataRepository from '../RealDataRepository'
import Pallets from '../PalletsRepository'
import AcceptanceChartGate from '../AcceptanceChartGateRepository'
import Alerts from '../AlertsRepository'
import OccupancyTiers from '../OccupancyTiersRepository'
import FreeTierCells from '../FreeTierCellsRepository'
import OccupancyCategories from '../OccupancyCategoriesRepository'
import FreeCategoryCells from '../FreeCategoryCellsRepository'
import AuthorityRepository from '../common/AuthorityRepository'
import ConfigRepository from '../common/ConfigRepository'

const repositories = {
  transportStatusRepository: TransportStatusRepository,
  acceptanceChartRepository: AcceptanceChartRepository,
  acceptanceScheduleRepository: AcceptanceScheduleRepository,
  utilsRepository: UtilsRepository,
  homeRepository: HomeRepository,
  acceptanceStatus: AcceptanceStatus,
  acceptanceTransportPUO: AcceptanceTransportPUO,
  acceptanceTransportSAP: AcceptanceTransportSAP,
  palletsRepository: Pallets,
  acceptanceChartGate: AcceptanceChartGate,
  alerts: Alerts,
  OccupancyTiers,
  FreeTierCells,
  OccupancyCategories,
  FreeCategoryCells,
  realDataRepository: RealDataRepository,
  authorityRepository: AuthorityRepository,
  occupancyTiers: OccupancyTiers,
  freeTierCells: FreeTierCells,
  occupancyCategories: OccupancyCategories,
  freeCategoryCells: FreeCategoryCells,
  configRepository: ConfigRepository
}

export const RepositoryFactory = {
  get: name => repositories[name]
}
