const devUrl = 'http://10.10.30.29:8080'

export const url =
    process.env.NODE_ENV === 'production' 
      ? window.location.origin
      : `${devUrl}/api` 

export const baseDomain =
    process.env.NODE_ENV === 'production' 
      ? `${url}/api`
      : `${devUrl}/api` 

export const apiVersion = 'v1'

export const TimeoutStatus = Object.freeze({'inLimit': 0, 'expired': 1})
export const timeoutInMilliseconds = 600000

const authDomain = window.location.origin
export const baseAuthDomain = `${authDomain}/api`
export const baseAuthDomainLogout = `${authDomain}`
