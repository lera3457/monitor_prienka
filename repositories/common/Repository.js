import axios from 'axios'
import { baseDomain, apiVersion, TimeoutStatus, timeoutInMilliseconds } from './DomainConstants'
import router from '../../router'

const baseURL = `${baseDomain}/${apiVersion}`

class AxiosWrapper {
  constructor () {
    let service = axios.create({
      baseURL: baseURL,
      timeout: timeoutInMilliseconds
    })

    service.interceptors.response.use(
      // set config + code if search results is empty
      config => {
        config.status === 204 ? this.noContent = true : this.noContent = false
        return config
      },
      (error) => {
        if (error.response.status === 403) {
          return router.push('error')
        } else if (error.code === 'ECONNABORTED') {
          console.log(`A timeout happend on url ${error.config.url}`)
          this.isRequestSuccess = TimeoutStatus.expired
        } else if (error.response && error.response.data && error.response.data.location && error.response.status === 302) {
          window.location = error.response.data.location
        }
      }
    )

    this.service = service
  }

  async get (path, params) {
    let returnedValue = { data: {}, isRequestSuccess: this.isRequestSuccess, noContent: this.noContent }
    try {
      const { data } = await this.service.get(path, params)
      this.isRequestSuccess = TimeoutStatus.inLimit

      returnedValue.data = data
      returnedValue.isRequestSuccess = this.isRequestSuccess
      returnedValue.noContent = this.noContent
    } catch (e) {
      returnedValue.isRequestSuccess = TimeoutStatus.expired
    }

    return returnedValue
  }

  async put (path, params) {
    let returnedValue = { data: {}, isRequestSuccess: this.isRequestSuccess }
    try {
      const { data } = await this.service.put(path, params)
      this.isRequestSuccess = TimeoutStatus.inLimit

      returnedValue.data = data
      returnedValue.isRequestSuccess = this.isRequestSuccess
    } catch (e) {
      returnedValue.isRequestSuccess = TimeoutStatus.expired
    }

    return returnedValue
  }
}

export default new AxiosWrapper()
