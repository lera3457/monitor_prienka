import { baseAuthDomain, adfsLogoutBaseUrl, apiVersion, timeoutInMilliseconds, baseAuthDomainLogout } from './DomainConstants.js'
import axios from 'axios'
import router from '../../router'

const getResource = '/authenticate'
const logoutResource = '/logout'

export default {
  get () {
    const baseURL = `${baseAuthDomain}/${apiVersion}`
    const service = axios.create({
      baseURL: baseURL,
      timeout: timeoutInMilliseconds
    })

    return service.get(`${getResource}`)
      .catch(function (error) {
        if (error.response.status === 403) {
          return router.push('error')
        } else if (error.response && error.response.data && error.response.data.location && error.response.status === 302) {
          window.location = error.response.data.location
        }
      })
  },
  logout: function (idToken) {
    const logoutRedirectURI = `${baseAuthDomainLogout}${logoutResource}`
    location.href = `${adfsLogoutBaseUrl}${logoutResource}` +
      `?id_token_hint=${idToken}&post_logout_redirect_uri=${logoutRedirectURI}`
  }
}
