import Repository from './common/Repository'
import qs from 'qs'

const resource = '/monitor_1_2_1'

export default {
  getDeliverySchedule () { return Repository.get(`${resource}`) },
  getStorage (queryParams) {
    return Repository.get(`${resource}`, {
      params: queryParams,
      paramsSerializer: params => {
        return qs.stringify(params, {arrayFormat: 'repeat'})
      }
    })
  }
}
