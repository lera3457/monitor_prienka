import { RepositoryFactory } from '../../repositories/common/ReposirotyFactory'

const AcceptanceChartRepository = RepositoryFactory.get('acceptanceChartRepository')
const UtilsRepository = RepositoryFactory.get('utilsRepository')

const state = {
  data: {},
  shift: []
}

const getters = {
  data: state => {
    return state.data
  },
  shift: state => {
    return state.shift
  }
}

const mutations = {
  setData: (state, payload) => {
    state.data = payload
  },
  setShift: (state, payload) => {
    state.shift = payload
  }
}

const actions = {
  getFilter: async (context, payload) => {
    const { data, isRequestSuccess } = await AcceptanceChartRepository.getStorage(payload.queryParams)

    context.commit('setTimeoutStatus', { isRequestSuccess: isRequestSuccess, currentPath: payload }, { root: true })

    if (isRequestSuccess !== 1) {
      context.commit('setData', data)
    }
  },
  getShift: async (context, payload) => {
    const { data, isRequestSuccess } = await UtilsRepository.getShift()

    context.commit('setTimeoutStatus', { isRequestSuccess: isRequestSuccess, currentPath: payload }, { root: true })

    if (isRequestSuccess !== 1) {
      context.commit('setShift', data)
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
