import { RepositoryFactory } from '../../repositories/common/ReposirotyFactory'
const TransportStatusRepository = RepositoryFactory.get('transportStatusRepository')

const state = {
  gridData: []
}

const getters = {
  gridData: state => {
    return state.gridData
  }
}

const mutations = {
  setData: (state, payload) => {
    state.gridData = payload
  }
}

const actions = {
  getFilter: async (context, payload) => {
    const { data, isRequestSuccess, noContent } = await TransportStatusRepository.getStorage(payload.queryParams)
    context.commit('setTimeoutStatus', { isRequestSuccess: isRequestSuccess, noContent: noContent, currentPath: payload }, { root: true })
    if (isRequestSuccess !== 1) {
      context.commit('setData', data)
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
