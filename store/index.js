import Vue from 'vue'
import Vuex from 'vuex'
import transportStatus from './modules/transportStatus'
import acceptanceSchedule from './modules/acceptanceSchedule'
import acceptanceChart from './modules/acceptanceChart'
import home from './modules/home'
import filterTable from './modules/filterTable'
import acceptanceStatus from './modules/acceptanceStatus'
import acceptanceTransportPUO from './modules/acceptanceTransportPUO'
import pallets from './modules/pallets'
import acceptanceTransportSAP from './modules/acceptanceTransportSAP'
import alerts from './modules/alerts'
import authority from './modules/authority'
import occupancyCategories from './modules/occupancyCategories'
import occupancyTiers from './modules/occupancyTiers'
import freeCategoryCells from './modules/freeCategoryCells'
import freeTierCells from './modules/freeTierCells'
import acceptanceChartGate from './modules/acceptanceChartGate'
import config from './modules/config'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    timeoutStatuses: [],
    currentPath: '',
    noContent: null
  },
  getters: {
    isFetchingSuccess: (state) => {
      return state.timeoutStatuses.reduce((a, b) => a + b, 0) === 0
    },
    isSearchResultsEmpty: (state) => {
      return state.noContent
    }
  },
  mutations: {
    setTimeoutStatus: (state, payload) => {
      if (state.currentPath !== payload.currentPath) {
        state.timeoutStatuses = []
      }

      let timeoutStatuses = state.timeoutStatuses
      timeoutStatuses.push(payload.isRequestSuccess)

      state.timeoutStatuses = timeoutStatuses
      state.currentPath = payload.currentPath
      // if search return 204
      if (typeof payload.noContent !== 'undefined') {
        state.noContent = payload.noContent
      }
    }
  },
  modules: {
    transportStatus,
    acceptanceSchedule,
    acceptanceChart,
    home,
    filterTable,
    acceptanceStatus,
    acceptanceTransportPUO,
    pallets,
    acceptanceTransportSAP,
    alerts,
    authority,
    occupancyCategories,
    occupancyTiers,
    freeTierCells,
    freeCategoryCells,
    acceptanceChartGate,
    config
  }
})
